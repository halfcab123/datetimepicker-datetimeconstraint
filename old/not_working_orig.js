
var initSessionDatePickers = function () {
 
    //$('#sessionstartpicker').change( ()=> $('#sessionendpicker').val( $('#sessionstartpicker').val() ) )
 
    //load session date pickers and constrain dates by conference start and end dates
    initSessionDatePicker($('#sessionstartpicker'), 'start');
    initSessionDatePicker($('#sessionendpicker'), 'end');
 
}
 
var startPickerThis
var endPickerThis
var startSelectedDate = null
var startSelectedTime = null
var endSelectedDate = null
var endSelectedTime = null
 
var setThis = function(ct, $i){
 
    if ($i.attr('id') == 'sessionstartpicker'){
        startPickerThis = this
    } else {
        endPickerThis = this
    }
}
 
var timeSelected = function (ct, $i) {
 
    var options = {}
 
    if ($i.attr('id') == 'sessionstartpicker') {
 
        startSelectedTime = moment(new Date($('#sessionstartpicker').val())).format('hh:mm A')
 
        console.log(ct.getDate())
        console.log(new Date(endSelectedDate).getDate())
        console.log(ct.getDate() == new Date(endSelectedDate).getDate())
 
        if (ct.getDate() == new Date(endSelectedDate).getDate()) {
            options.maxTime = endSelectedTime
            console.log(JSON.stringify(options))
 
            this.setOptions(options)
        }
    } else {
 
        endSelectedTime = moment(new Date($('#sessionendpicker').val())).add(15, 'minutes').format('hh:mm A')
 
        console.log(ct.getDate())
        console.log(new Date(startSelectedDate).getDate())
        console.log(ct.getDate() == new Date(startSelectedDate).getDate())
 
        if (ct.getDate() == new Date(startSelectedDate).getDate()) {
            options.minTime = startSelectedTime
            console.log(JSON.stringify(options))
 
            this.setOptions(options)
        }
    }
}
 
var timeConstraint = function (ct, $i) {
 
    console.log('selection change or show')
    var startTime = moment(new Date($('#confstartdate').val())).format('hh:mm A')
 
    //because the end date is showing 1 hour and 15 seconds earlier then is set... ???
    var endTime = moment(new Date($('#confenddate').val())).add(15, 'minutes').format('hh:mm A')
 
    var startDate = new Date($('#confstartdate').val())
    var endDate = new Date($('#confenddate').val())
    var sameDay = startDate.getDate() == endDate.getDate()
 
    //default options for days between start and end days
    var options = {
        //defaultTime : '09:00 AM',
        minTime: '08:00 AM',
        maxTime: '10:15 PM'
    }
 
    //If the startdate and enddate of the conference are the same day
    if (sameDay && startDate.getDate() == ct.getDate()) {
        //options.defaultTime = startTime
        options.minTime = startTime
        options.maxTime = endTime
    }
 
    //If the default or selected day is the start day
    if (!sameDay && startDate.getDate() == ct.getDate()) {
        options.minTime = startTime
        options.maxTime = '10:15 PM'
        
    }
 
    //If the default or selected day is the end day
    if (!sameDay && endDate.getDate() == ct.getDate()) {
        options.minTime = '08:00 AM'
        options.maxTime = endTime
       
    }
 
    var endOpts = {}
    var startOpts = {}
 
    if ($i.attr('id') == 'sessionstartpicker') {
 
        startSelectedDate = moment(new Date($('#sessionstartpicker').val())).format('MM/DD/YYYY')
        startSelectedTime = moment(new Date($('#sessionstartpicker').val())).format('hh:mm A')
 
        endOpts = {
            //defaultDate: moment(new Date($('#sessionstartpicker').val())).format('MM/DD/YYYY'),
            minDate: startSelectedDate,
            minTime: startSelectedTime
        }
 
        if (ct.getDate() == new Date(endSelectedDate).getDate()) {
            options.maxTime = endSelectedTime
            console.log(JSON.stringify(options))
        }
 
        endPickerThis.setOptions(endOpts)
 
    } else {
 
        endSelectedDate = moment(new Date($('#sessionendpicker').val())).format('MM/DD/YYYY')
        endSelectedTime = moment(new Date($('#sessionendpicker').val())).add(15, 'minutes').format('hh:mm A')
 
        startOpts = {
            //defaultDate: moment(new Date($('#sessionendpicker').val())).format('MM/DD/YYYY'),
            maxDate: endSelectedDate,
            maxTime: endSelectedTime
        }
 
        if (ct.getDate() == new Date(startSelectedDate).getDate()) {
            options.minTime = startSelectedTime
            console.log(JSON.stringify(options))
        }
  
        startPickerThis.setOptions(startOpts)
    }
 
    console.log(JSON.stringify(options))
    //set datetimepicker options using options object
    this.setOptions(options)
 
    //Output all options object values to console to check values
    //console.log('Start and End Date are the same?: ' + sameDay)
    //console.log('Default Selected Time: ' + options.defaultTime)
    //console.log('minTime: ' + options.minTime)
    //console.log('maxTime: ' + options.maxTime)
 
}
 
function initSessionDatePicker(el, startOrEnd) {
 
    var startDate = $('#sessionstartdate').val().indexOf('/1970 ') >= 0 ? new Date($('#confstartdate').val()) : new Date($('#sessionstartdate').val())
    var endDate = $('#sessionenddate').val().indexOf('/1970 ') >= 0 ? new Date($('#confenddate').val()) : new Date($('#sessionenddate').val())
    var startDateString = moment(startDate).format('MM/DD/YYYY')
    var endDateString = moment(endDate).format('MM/DD/YYYY')
    var selectedDate = startOrEnd == 'start' ? startDate : endDate
 
    el.datetimepicker({
        defaultDate: selectedDate,
        format: 'm/d/Y h:i A',
        step: 15,
        formatDate: 'm/d/Y',
        formatTime: 'h:i A',
        minDate: startDateString,
        maxDate: endDateString,
        onShow: timeConstraint,
        onSelectDate: timeConstraint,
        onSelectTime: timeSelected,
        onGenerate: setThis
    });
 
    if (startOrEnd == 'start') {
        el.attr('value', moment(startDate).format('MM/DD/YYYY hh:mm A'))
    } else {
        el.attr('value', moment(endDate).format('MM/DD/YYYY hh:mm A'))
    }
 
}
