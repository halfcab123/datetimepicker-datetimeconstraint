# DateTimePicker-DateTimeConstraint

This project was designed to constrain 2 DateTimePickers to each other. There are two versions of the logic. 

**v1** will not allow the start picker to pick a date/time after the endpicker, therefore only showing date/times before the endpicker's current date/time.

**v2** has less code as it will allow you to choose any date/time within the default constraint, however it will force the other picker's value to move to a date/time that is within keeping of the start and end picker relationship
