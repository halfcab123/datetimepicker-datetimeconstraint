function getVars() {
    this.confStartDate = $('[data-session-dates="confstartdate"]').attr("value")
    this.confEndDate = $('[data-session-dates="confenddate"]').attr("value")
    this.sessionStartDate = new Date($('[data-session-dates="sessionstartdate"]').attr("value")).getFullYear() == 1970 ?
        null : $('[data-session-dates="sessionstartdate"]').attr("value")
    this.sessionEndDate = new Date($('[data-session-dates="sessionenddate"]').attr("value")).getFullYear() == 1970 ?
        null : $('[data-session-dates="sessionenddate"]').attr("value")

    this.$startPicker = $('[data-sessionstartpicker]')
    this.$endPicker = $('[data-sessionendpicker]')

    this.test = $('[data-session-dates="confstartdate"]').attr("value")

    //remove attributes in case of multiple use
    $('[data-session-dates]').removeAttr('data-session-dates')
    $startPicker.removeAttr('data-sessionstartpicker')
    $endPicker.removeAttr('data-sessionendpicker')

    return this
}

var vars = getVars()

function toDate(date) {
    //date type
    this.dt = new Date(date)

    //string values
    this.year = this.dt.getFullYear()
    this.day = this.dt.getDate()

    //time formatted as string
    this.time = moment(this.dt).format("hh:mm A")
    this.date = moment(this.dt).format("MM/DD/YYYY")
    this.fullDate = this.date + " " + this.time

    this.maxTime = moment(this.dt).add(15, "minutes").format("hh:mm A")

    return this
}


function endDateSelected(currentDate, $i) {

    var startPickerValue = vars.$startPicker.val()

    var options = {  //default times
        minTime: "08:00 AM",
        maxTime: "10:15 PM",
        value: toDate(currentDate).fullDate //refresh selected date
    }

    vars.$startPicker.datetimepicker({ //min date for endpicker set to selected start date
        maxDate: toDate(currentDate).date
    })

    //if end picker day matches conf end day
    //endpicker maxTime = confEnd Time
    if (currentDate.getDate() == toDate(vars.confEndDate).day) {
        options.maxTime = toDate(vars.confEndDate).maxTime //add 15 min
    }

    //if end picker day matches start picker day
    //endpicker minTime = startpicker current time
    if (currentDate.getDate() == toDate(startPickerValue).day) {
        options.minTime = toDate(startPickerValue).time
    }

    //If endpicker day matches confstartdate it will also match
    //start picker day, so that case is handled above 
    vars.$endPicker.datetimepicker(options)

}

function startDateSelected(currentDate, $i) {

    //min date for endpicker set to selected start date
    vars.$endPicker.datetimepicker({ minDate: toDate(currentDate).date })

    var endPickerValue = vars.$endPicker.val()

    //default times
    //min and max times for selected day that is not start or
    //end day of conference
    var options = {
        minTime: "08:00 AM",
        maxTime: "10:15 PM",
        value: toDate(currentDate).fullDate
    }

    //if start picker day matches conf start day
    //startpicker minTime = confStart time
    if (currentDate.getDate() == toDate(vars.confStartDate).day) {
        options.minTime = toDate(vars.confStartDate).time
    }

    //if startpicker day matches endpicker day
    //startpicker maxtime = endpicker current time
    if (currentDate.getDate() == toDate(endPickerValue).day) {
        options.maxTime = toDate(endPickerValue).time
    }

    //If startpicker day matches confenddate it will also match
    //end picker day, so that case is handled above 
    vars.$startPicker.datetimepicker(options)
}



function initSessionDatePickers() {

    var defaultOpts = {
        format: 'm/d/Y h:i A',
        step: 15,
        formatDate: 'm/d/Y',
        formatTime: 'h:i A'
    }

    var startOptions = {
        ...defaultOpts, //merge objs
        minDate: vars.confStartDate,
        maxDate: vars.sessionEndDate ?? vars.confEndDate,
        value: vars.sessionStartDate ?? vars.confStartDate,
        onSelectDate: startDateSelected,
        onShow: startDateSelected
    }

    vars.$startPicker.datetimepicker(startOptions)

    var endOptions = {
        ...defaultOpts, //merge objs
        minDate: vars.sessionStartDate ?? vars.confStartDate,
        maxDate: vars.confEndDate,
        value: vars.sessionEndDate ?? vars.confEndDate,
        onSelectDate: endDateSelected,
        onShow: endDateSelected

    }

    vars.$endPicker.datetimepicker(endOptions)

}

initSessionDatePickers()
