function getVars() {
    this.confStartDate = $('[data-session-dates="confstartdate"]').attr("value")
    this.confEndDate = $('[data-session-dates="confenddate"]').attr("value")
    this.sessionStartDate = new Date($('[data-session-dates="sessionstartdate"]').attr("value")).getFullYear() == 1970 ?
        null : $('[data-session-dates="sessionstartdate"]').attr("value")
    this.sessionEndDate = new Date($('[data-session-dates="sessionenddate"]').attr("value")).getFullYear() == 1970 ?
        null : $('[data-session-dates="sessionenddate"]').attr("value")

    this.$startPicker = $('[data-sessionstartpicker]')
    this.$endPicker = $('[data-sessionendpicker]')

    this.test = $('[data-session-dates="confstartdate"]').attr("value")

    //remove attributes in case of multiple use
    $('[data-session-dates]').removeAttr('data-session-dates')
    $startPicker.removeAttr('data-sessionstartpicker')
    $endPicker.removeAttr('data-sessionendpicker')

    return this
}

var vars = getVars()

function toDate(date) {
    //date type
    this.dt = new Date(date)

    //string values
    this.year = this.dt.getFullYear()
    this.day = this.dt.getDate()

    //time formatted as string
    this.time = moment(this.dt).format("hh:mm A")
    this.date = moment(this.dt).format("MM/DD/YYYY")
    this.fullDate = this.date + " " + this.time

    this.maxTime = moment(this.dt).add(15, "minutes").format("hh:mm A")

    return this
}


function dateTimeSelected(cd, $i) {

    if (cd.getDate() == new Date(vars.confStartDate).getDate()) {
        $i.datetimepicker({
            minTime: moment(new Date(confStartDate)).format("hh:mm A"),
            maxTime: "10:15 PM",
            value: $i.val()
        })
    }

    if (cd.getDate() == new Date(vars.confEndDate).getDate()) {
        $i.datetimepicker({
            minTime: "07:00 AM",
            maxTime: moment(new Date(confEndDate)).add(15, "minutes").format("hh:mm A"),
            value: $i.val()
        })
    }

    if (new Date(vars.$endPicker.val()) < new Date(vars.$startPicker.val())) {
        vars.$endPicker.val(vars.$startPicker.val())
    }

    if (new Date(vars.$startPicker.val()) > new Date(vars.$endPicker.val())) {
        vars.$startPicker.val(vars.$endPicker.val())
    }
}



function initSessionDatePickers() {

    var startOptions = {
        format: 'm/d/Y h:i A',
        step: 15,
        formatDate: 'm/d/Y',
        formatTime: 'h:i A',
        minDate: vars.confStartDate,
        maxDate: vars.confEndDate,
        value: vars.sessionStartDate ?? vars.confStartDate,
        onSelectDate: dateTimeSelected,
        onShow: dateTimeSelected,
        onSelectTime: dateTimeSelected
    }

    vars.$startPicker.datetimepicker(startOptions)

    var endOptions = {
        format: 'm/d/Y h:i A',
        step: 15,
        formatDate: 'm/d/Y',
        formatTime: 'h:i A',
        minDate: vars.confStartDate,
        maxDate: vars.confEndDate,
        value: vars.sessionEndDate ?? vars.confEndDate,
        onSelectDate: dateTimeSelected,
        onShow: dateTimeSelected,
        onSelectTime: dateTimeSelected

    }

    vars.$endPicker.datetimepicker(endOptions)

}

initSessionDatePickers()
